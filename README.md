# Usage
```console
        $ docker build -t nginx-multistage .
        $ docker run -d -v $(pwd)/nginx/nginx.conf:/etc/nginx/nginx.conf nginx-multistage
```
