FROM debian:9 as build-stage

RUN apt-get update && apt-get install -y \
	build-essential \
	libpcre++-dev \
	wget \
	zlib1g-dev

WORKDIR /tmp

# download nginx
RUN wget http://nginx.org/download/nginx-1.11.10.tar.gz \
	&& tar xzf nginx-1.11.10.tar.gz

# download LuaJIT
RUN wget http://luajit.org/download/LuaJIT-2.0.4.tar.gz \
	&& tar xzf LuaJIT-2.0.4.tar.gz

# download nginx devel kit
RUN wget -O nginx_devel_kit.tar.gz \
	https://github.com/simpl/ngx_devel_kit/archive/v0.3.0.tar.gz \
	&& tar xzf nginx_devel_kit.tar.gz

# download nginx lua module
RUN wget -O nginx_lua_module.tar.gz \
	https://github.com/openresty/lua-nginx-module/archive/v0.10.8.tar.gz \
	&& tar xzf nginx_lua_module.tar.gz

WORKDIR /tmp/LuaJIT-2.0.4

RUN make install

WORKDIR /tmp/nginx-1.11.10

RUN LUAJIT_LIB=/usr/local/lib LUAJIT_INC=/usr/local/include/luajit-2.0 \
	./configure \
	--add-module=/tmp/ngx_devel_kit-0.3.0 \
	--add-module=/tmp/lua-nginx-module-0.10.8 \
	&& make install


FROM debian:9 as run-stage

COPY --from=build-stage /usr/local/lib/libluajit-5.1.so.2 /usr/lib/
COPY --from=build-stage /usr/local/nginx /usr/local/nginx

EXPOSE 80

CMD /usr/local/nginx/sbin/nginx -c /etc/nginx/nginx.conf -g "daemon off;"
